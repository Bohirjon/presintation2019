﻿using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Autofac;
using DependencyInjection.Abstractions;
using DependencyInjection.Services;
using DependencyInjection.ViewModel;

namespace DINetCorePlatform
{
    class Program
    {
        static async Task Main(string[] args)
        {
            #region Hard to create instance
            var navigationService = new NavigationService(new AuthenticationService());
            await navigationService.InitMainPageAsync();
            #endregion

            #region Hard to create instance
            /*
             * Somewhere we want to create an instance of LoginVieWModel and it's gonna look: 
             */
            var loginViewModel = new LoginViewModel(
                new AuthenticationService(),
                new NavigationService(
                    new AuthenticationService()),
                new DialogService());
            #endregion

            #region DI_Container

            #region BuildingDependencies
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<LoginViewModel>().AsSelf();
            containerBuilder.RegisterType<MainViewModel>().AsSelf();
            containerBuilder.RegisterType<ChatViewModel>().AsSelf();
            containerBuilder.RegisterType<ConversationViewModel>().AsSelf();
            containerBuilder.RegisterType<NotificationManager>().AsSelf();


            containerBuilder.RegisterType<AuthenticationService>().As<IAuthenticationService>();
            containerBuilder.RegisterType<DialogService>().As<IDialogService>();
            containerBuilder.RegisterType<NavigationService>().As<INavigationService>();
            containerBuilder.RegisterType<ChatService>().As<IChatService>().SingleInstance();
            var container = containerBuilder.Build();
            #endregion

            #region Example1
            //Resolving 
            //Asking DI" container to resolve
            var navigationService1 = container.Resolve<INavigationService>();

            await navigationService1.InitMainPageAsync();
            #endregion

            #region Example2

            var loginViewModel1 = container.Resolve<LoginViewModel>();

            #endregion

            #endregion
        }
    }

}
