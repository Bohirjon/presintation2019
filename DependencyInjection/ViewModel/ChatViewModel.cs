﻿using System;
using System.Collections.ObjectModel;
using DependencyInjection.Abstractions;
using DependencyInjection.Dto;

namespace DependencyInjection.ViewModel
{
    public class ChatViewModel
    {
        public ObservableCollection<MessageDto> Messages { get; set; }

        public ChatViewModel(IChatService chatService)
        {
            chatService.MessageReceived
                .Subscribe(x => Messages.Add(x));
        }
    }
}