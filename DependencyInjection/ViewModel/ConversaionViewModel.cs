﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using DependencyInjection.Abstractions;

namespace DependencyInjection.ViewModel
{
    public class ConversationViewModel
    {

        public ObservableCollection<string> Chat { get; set; } = new ObservableCollection<string>();

        public ConversationViewModel(IChatService chatService)
        {

            chatService.MessageReceived
                .Select(dto => dto.From)
                .Subscribe(from =>
                {
                    if (Chat.All(s => s == from))
                    {
                        Chat.Add(from);
                    }
                });
        }

    }
}