﻿using System.Threading.Tasks;
using DependencyInjection.Abstractions;
using DependencyInjection.Dto;

namespace DependencyInjection.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly INavigationService _navigationService;
        private readonly IDialogService _dialogService;
        public string Login { get; set; }
        public string Password { get; set; }

        public LoginViewModel(IAuthenticationService authenticationService
        , INavigationService navigationService
        , IDialogService dialogService)
        {
            _authenticationService = authenticationService;
            _navigationService = navigationService;
            _dialogService = dialogService;
        }

        private async Task SignIn()
        {
            var signDto = new SignDto
            {
                Login = Login,
                PasswordEncrypted = Password.Has()
            };
            var authenticationResult = await _authenticationService.SignIn(signDto);
            if (authenticationResult.IsSuccess)
            {
                await _navigationService.Navigate<MainViewModel>();
            }
            else
            {
                await _dialogService.Alert("Authentication failed", authenticationResult.Message);
            }
        }
    }
}