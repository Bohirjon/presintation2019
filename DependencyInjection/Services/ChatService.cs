﻿using System;
using System.Net.Http;
using System.Reactive.Subjects;
using DependencyInjection.Abstractions;
using DependencyInjection.Dto;

namespace DependencyInjection.Services
{
    public class ChatService : IChatService
    {
        private void OnMessageReceived(MessageDto message)
        {
            _messageSubject.OnNext(message);
        }
        public ChatService()
        {
            var httpClient = new HttpClient();
            //listen to server with http then event to subject
            _messageSubject = new Subject<MessageDto>();
            MessageReceived = _messageSubject;
        }

        private readonly Subject<MessageDto> _messageSubject;
        public IObservable<MessageDto> MessageReceived { get; }
    }
}