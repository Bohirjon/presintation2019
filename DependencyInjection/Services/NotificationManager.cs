﻿using System;
using System.Reactive.Linq;
using DependencyInjection.Abstractions;

namespace DependencyInjection.ViewModel
{
    public class NotificationManager
    {
        private void Notify(string title, string content)
        {
            //notify
        }
        public NotificationManager(IChatService chatService)
        {
            chatService.MessageReceived
                .Where(dto => dto.IsImportant)
                .Subscribe(message => Notify(message.From, message.Message));
        }
    }
}