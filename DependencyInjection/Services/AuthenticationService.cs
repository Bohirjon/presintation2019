﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using DependencyInjection.Abstractions;
using DependencyInjection.Dto;

namespace DependencyInjection.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly HttpClient _httpClient;
        public AuthenticationService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://mywebservice.com/demo");

        }
        public async Task<AuthenticationResult> SignIn(SignDto signDto)
        {
            var json = JsonSerializer.Serialize(signDto);
            var httpResponseMessage = await _httpClient.PostAsync("signin", new StringContent(json));
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var jsonResult= await httpResponseMessage.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<AuthenticationResult>(jsonResult);
                return result;
            }

            return new AuthenticationResult
            {
                IsSuccess = false,
                Message = httpResponseMessage.ReasonPhrase,
            };
        }
        public Task<bool> IsAuthenticated()
        {
            //some code that touches the environment
            //Appsettings: see if the app has logged in ...
            return Task.FromResult(true);
        }
    }
}