﻿using System;
using System.Threading.Tasks;
using DependencyInjection.Abstractions;

namespace DependencyInjection.Services
{
    public class DialogService : IDialogService
    {
        public Task Alert(string title, string message)
        {
            //It could be console or any platform 
            Console.WriteLine(message);
            return Task.FromResult(Console.ReadKey(true));
        }
    }
}