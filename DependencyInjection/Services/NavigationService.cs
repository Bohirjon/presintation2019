﻿using System.Threading.Tasks;
using DependencyInjection.Abstractions;
using DependencyInjection.ViewModel;

namespace DependencyInjection.Services
{
    public class NavigationService : INavigationService
    {
        private readonly IAuthenticationService _authenticationService;
        public NavigationService(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }
        public async Task InitMainPageAsync()
        {
            if (await _authenticationService.IsAuthenticated())
            {
                var mainViewModels = new MainViewModel();
                await Navigate(mainViewModels);
            }
            else
            {
                var loginViewModel = new LoginViewModel(
                    _authenticationService
                    , this
                    , new DialogService());
                await Navigate(loginViewModel);
            }
        }
        public Task Navigate(BaseViewModel baseViewModel)
        {
            //Navigation face that's in domain level 
            //Question how would u do this ?
            //in this case DI comes to help
            return Task.CompletedTask;
        }
        /// <summary>
        /// This touches domain level environment
        /// so then it should also use abstraction. navigationFacade 
        /// </summary>
        /// <typeparam name="TBaseViewModel"></typeparam>
        /// <returns></returns>
        public Task Navigate<TBaseViewModel>() where TBaseViewModel : BaseViewModel
        {
            /*
             * here the DI comes to
             * in this case as you see we are not in a domain level that means we can't
             * touch to the navigation stuff in this proj
             * Solution: DI all pages or windows most be registered into IoC as their view Models
             * and we could easily resolve the pages or windows then navigating them navigation facade
             * (that should so be registered into IoC)
             */
            return Task.CompletedTask;
        }

    }
}