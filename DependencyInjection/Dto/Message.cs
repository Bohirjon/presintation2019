﻿namespace DependencyInjection.Dto
{
    public class MessageDto
    {
        public bool IsImportant { get; set; }
        public string From { get; set; }
        public string ToConversation { get; set; }
        public string Message { get; set; }
    }
}