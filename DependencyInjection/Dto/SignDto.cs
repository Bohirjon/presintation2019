﻿namespace DependencyInjection.Dto
{
    public class SignDto
    {
        public string Login { get; set; }
        /// <summary>
        /// As u know we won't pass the passwords in connections
        /// first encrypt with the same algorithms with server
        /// then pass it. :-)
        /// </summary>
        public string PasswordEncrypted { get; set; }
    }
}