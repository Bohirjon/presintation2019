﻿using System;

namespace DependencyInjection.Dto
{
    public class AuthenticationResult
    {
        public bool IsSuccess { get; set; }
        public string Token { get; set; }
        public string Message { get; set; }
        public DateTime TokenExpires { get; set; }
    }
}