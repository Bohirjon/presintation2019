﻿using System.Threading.Tasks;
using DependencyInjection.ViewModel;

namespace DependencyInjection.Abstractions
{
    public interface INavigationService
    {
        Task InitMainPageAsync();
        Task Navigate<TBaseViewModel>() where TBaseViewModel : BaseViewModel;
    }
}