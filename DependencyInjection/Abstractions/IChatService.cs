﻿using System;
using DependencyInjection.Dto;
using ReactiveUI;

namespace DependencyInjection.Abstractions
{
    public interface IChatService
    {
        IObservable<MessageDto> MessageReceived { get; }
    }
}