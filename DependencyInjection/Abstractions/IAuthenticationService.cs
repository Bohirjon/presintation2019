﻿using System.Threading.Tasks;
using DependencyInjection.Dto;

namespace DependencyInjection.Abstractions
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResult> SignIn(SignDto signDto);
        Task<bool> IsAuthenticated();
    }
}