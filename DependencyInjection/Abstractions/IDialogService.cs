﻿using System.Threading.Tasks;

namespace DependencyInjection.Abstractions
{
    public interface IDialogService
    {
        Task Alert(string title, string message);
    }
}